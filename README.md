# ABLabs Static Site
Currently, this is just a placeholder when visiting https://www.ablabs.io.

## Gitlab Pages
This site is using Gitlab Pages to generate a static HTML page. The `.gitlab-ci.yml` file will create an artifact called `public` which will serve the static content found in the `public` folder.

## Making Changes
Simply update the static content found in the Public folder and merge to the `master` branch. The pipeline will run, create an artifact and serve the content at the domain set in the pages configuration.